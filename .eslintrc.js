module.exports = {
  "extends": "google",
  "parser": "babel-eslint",
  "env": {
    "browser": true,
    "node": true,
    "serviceworker": true
  },
  "rules": {
    "strict": 0,
    "linebreak-style":"off",
    "semi": "off",
    "valid-jsdoc": "off"
  }
};