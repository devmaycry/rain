let drawInterval = null;
const ctxTimer = null;
let speedCount = [];

const letitrain = (options = null) => {
  window.clearInterval(drawInterval);
  window.clearTimeout(ctxTimer);
  let defaults = {
    speed: 40,
    fontSize: 10,
    rainLength: 15,
    fontFamily: 'MS Gothic,arial',
    fontWeight: 'medium',
    leadFontWeight: 'bold',
    leadColor: '#ff9999', // b0ffee
    fontColor: '#e60000', // 0f0
    shadowColor: '#ffe6e6',
    shadowBlur: 10,
    maxSpeed: 4,
    titleText: 'DEV MAY CRY',
  };

  // settings
  if (options !== null) {
    defaults = {
      ...defaults,
      options,
    };
  }

  const {
    speed, fontSize, fontFamily, fontWeight, rainLength,
    leadFontWeight, leadColor, fontColor, shadowColor, shadowBlur,
    maxSpeed,
  } = defaults;

  const rain = document.getElementById('matrix-rain');
  const ctx = rain.getContext('2d');

  const lead = document.getElementById('matrix-lead');
  const leadctx = lead.getContext('2d');

  const title = document.getElementById('matrix-title');
  // const titlectx = rain.getContext('2d');

  // making the canvas full screen
  rain.height = window.innerHeight;
  rain.width = window.innerWidth;

  lead.height = window.innerHeight;
  lead.width = window.innerWidth;

  title.height = window.innerHeight;
  title.width = window.innerWidth;

  // inventory characters - taken from the unicode charset
  const inventory = ['ア', 'イ', 'ウ', 'エ', 'オ',
    'カ', 'キ', 'ク', 'ケ', 'コ',
    'サ', 'シ', 'ス', 'セ', 'ソ',
    'タ', 'チ', 'ツ', 'テ', 'ト',
    'ナ', 'ニ', 'ヌ', 'ネ', 'ノ',
    'ハ', 'ヒ', 'フ', 'ヘ', 'ホ',
    'マ', 'ミ', 'ム', 'メ', 'モ',
    'ヤ', 'ユ', 'ヨ', ' ',
    'ラ', 'リ', 'ル', 'レ', 'ロ',
    'ワ', 'ヰ', 'ヱ', 'ヲ',
    'ン', '゛', '゜', 'ー', 'ッ',
    '0,', '1', '2', '3', '4', '5', '6', '7', '8', '9',
    'H', 'a', 'C', 'e', 'r', '#',
    '"', ';', '|', '(', ')', '\\', '/', '*',
    '<', '>', '@', '#', '$', '%', '^', '&', '-', '+', '`',
  ];

  // an array of drops - one per column
  let drops = mapDrops(rain, lead, fontSize);
  speedCount = initSpeedCount(rain, fontSize);

  /**
   * Create the rain map
   */
  function mapDrops(rain, lead, fontSize) {
    const newColumns = rain.width / fontSize;
    const newRows = rain.height / fontSize;
    const drops = [];
    for (let x = 0; x < newColumns; x += 1) {
      drops.push({
        start: Math.floor(Math.random() * -newRows),
        speed: Math.floor(Math.random() * maxSpeed) + 1,
      });
    }
    rain.height = window.innerHeight;
    rain.width = window.innerWidth;

    lead.height = window.innerHeight;
    lead.width = window.innerWidth;
    return drops;
  }

  /**
   * Set initial value for speedCount
   *
   * @param {*} rain
   * @param {*} fontSize
   */
  function initSpeedCount(rain, fontSize) {
    const newColumns = rain.width / fontSize;
    const speedMap = [];
    for (let x = 0; x < newColumns; x += 1) {
      speedMap.push(0);
    }
    return speedMap;
  }

  /**
   * getText()
   *
   * generate a random string based on speed
   * @param array inventory
   * @param int speed
   */
  function getText(inventory) {
    const randomIndex = Math.floor((Math.random() * (inventory.length -1)));
    const text = inventory[randomIndex];
    return text;
  }

  /**
  * Rrawing the characters
  * @constructor
  */
  function draw() {
    // reset lead board
    if (rain.width < window.innerWidth) {
      drops = mapDrops(rain, lead, fontSize);
    }

    ctx.fillStyle = fontColor; // green text
    ctx.font = `${fontWeight} ${fontSize}px ${fontFamily}`;

    leadctx.fillStyle = leadColor; // green text
    leadctx.font = `${leadFontWeight} ${fontSize}px ${fontFamily}`;

    // looping over drop columns
    for (let i = 0; i < drops.length; i += 1) {
      speedCount[i] += 1;

      if (speedCount[i] === drops[i].speed) {
        // console.log(speedCount[i], drops[i].speed );
        speedCount[i] = 0;

        // a random inventory character to print
        const text = getText(inventory);
        // x = i*fontSize, y = value of drops[i]*fontSize
        const thisX = i * fontSize;
        const thisY = drops[i].start * fontSize;
        const leadY = thisY + fontSize;


        ctx.fillStyle = fontColor; // green text
        ctx.shadowColor = '#ff8080'; // string
        ctx.shadowOffsetX = 0; // integer
        ctx.shadowOffsetY = 0; // integer
        ctx.shadowBlur = 2; // integer

        ctx.fillText(text, thisX, thisY);

        ctx.clearRect( thisX, 0, fontSize,
            (leadY - Math.floor(rainLength * fontSize * 2)));
        ctx.clearRect( thisX, thisY + fontSize,
            fontSize, rainLength * fontSize);

        leadctx.clearRect(thisX - 3, 0, fontSize + 6, thisY + fontSize);

        leadctx.fillStyle = leadColor;
        leadctx.shadowColor = shadowColor; // string
        leadctx.shadowOffsetX = 0; // integer
        leadctx.shadowOffsetY = 0; // integer
        leadctx.shadowBlur = shadowBlur; // integer
        leadctx.fillText(text, thisX, thisY + fontSize);

        if (leadY > rain.height && Math.random() > 0.975) {
          drops[i].start = 0;
        }
      }
      // incrementing Y coordinate
      drops[i].start += 1 / drops[i].speed;
    }
  }

  drawInterval = setInterval(draw, speed);
};


export default letitrain;
