let timeInterval;

const updateTime = () => {
  clearInterval(timeInterval);

  function getDateTime() {
    const d = new Date();
    const months = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN',
      'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];
    const days = ['MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN'];
    let h = d.getHours();
    let m = d.getMinutes();
    let s = d.getSeconds();
    // const M = months[d.getMonth()];
    // const D = days[d.getDay()];
    // const dd = d.getDate();
    let A = 'AM';
    if (h > 12) {
      h = h - 12;
      A = 'PM';
    }
    if (h < 10) {
      h = '0' + h;
    }
    if (m < 10) {
      m = '0' + m;
    }
    if (s < 10) {
      s = '0' + s;
    }

    const time = `${h}:${m}:${s}`;
    // date = `${M} ${dd} ${D}`;
    document.getElementById('time').innerHTML = time;
    return false;
  }
  timeInterval = setInterval(getDateTime, 1000);
}
export default updateTime;
