let drawInterval = null;
let timeInterval = null;

const getDateTime = () => {
  const d = new Date();
  const months = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN',
    'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];
  const days = ['MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN'];
  let h = d.getHours();
  let m = d.getMinutes();
  let s = d.getSeconds();
  // const M = months[d.getMonth()];
  // const D = days[d.getDay()];
  // const dd = d.getDate();
  let A = 'AM';
  if (h > 12) {
    h = h - 12;
    A = 'PM';
  }
  if (h < 10) {
    h = '0' + h;
  }
  if (m < 10) {
    m = '0' + m;
  }
  if (s < 10) {
    s = '0' + s;
  }

  const time = `${h}:${m}:${s}`;
  // date = `${M} ${dd} ${D}`;
  document.getElementById('time').innerHTML = time;
  return time;
}

timeInterval = setInterval(getDateTime, 1000);

const letitrain = (options = null) => {
  window.clearInterval(drawInterval);

  let defaults = {
    speed: 50,
    fontSize: 10,
    rainLength: 40,
    fontFamily: 'MS Gothic,arial',
    fontWeight: 'medium',
    leadFontWeight: 'bold',
    leadColor: '#ff9999', // b0ffee
    fontColor: '#e60000', // 0f0
    shadowColor: '#ffe6e6',
    shadowBlur: 10,
    titleText: 'DEV MAY CRY',
  };

  // settings
  if (options !== null) {
    defaults = {
      ...defaults,
      options,
    };
  }

  const {
    speed, fontSize, fontFamily, fontWeight, rainLength,
    leadFontWeight, leadColor, fontColor, shadowColor, shadowBlur, titleText,
  } = defaults;

  const rain = document.getElementById('matrix-rain');
  const ctx = rain.getContext('2d');

  const lead = document.getElementById('matrix-lead');
  const leadctx = lead.getContext('2d');

  const title = document.getElementById('matrix-title');
  // const titlectx = rain.getContext('2d');

  // making the canvas full screen
  rain.height = window.innerHeight;
  rain.width = window.innerWidth;

  lead.height = window.innerHeight;
  lead.width = window.innerWidth;

  title.height = window.innerHeight;
  title.width = window.innerWidth;

  // inventory characters - taken from the unicode charset
  const inventory = ['ア', 'イ', 'ウ', 'エ', 'オ',
    'カ', 'キ', 'ク', 'ケ', 'コ',
    'サ', 'シ', 'ス', 'セ', 'ソ',
    'タ', 'チ', 'ツ', 'テ', 'ト',
    'ナ', 'ニ', 'ヌ', 'ネ', 'ノ',
    'ハ', 'ヒ', 'フ', 'ヘ', 'ホ',
    'マ', 'ミ', 'ム', 'メ', 'モ',
    'ヤ', 'ユ', 'ヨ', ' ',
    'ラ', 'リ', 'ル', 'レ', 'ロ',
    'ワ', 'ヰ', 'ヱ', 'ヲ',
    'ン', '゛', '゜', 'ー', 'ッ',
    '0,', '1', '2', '3', '4', '5', '6', '7', '8', '9',
    'H', 'a', 'C', 'e', 'r', '#',
    '"', ';', '|', '(', ')', '\\', '/', '*',
    '<', '>', '@', '#', '$', '%', '^', '&', '-', '+', '`',
  ];

  // an array of drops - one per column
  let drops = mapDrops(rain, lead, fontSize, []);

  let titleProgress = 0;
  // const titleMap = getTitleMap(titleText);

  /**
   * Find Title
   * @constructor
   */
  // function getTitleMap(text) {
  //   const columns = Math.ceil(title.width / fontSize);
  //   const center = Math.ceil(title.width / 2 / fontSize);
  //   const textHalfLength = Math.ceil(text.length / 2);
  //   const titleBegin = center - textHalfLength;
  //   const y = Math.ceil(title.height / fontSize / 2);
  //   const titleMap = [];
  //   for (let t = 0; t <= text.length; t += 1) {
  //     const x = titleBegin + t;
  //     const xFlipped = columns - x;
  //     titleMap[xFlipped] = y;
  //   }
  //   return titleMap;
  // }

  /**
   * Create the rain map
   */
  function mapDrops(rain, lead, fontSize, drops) {
    const startIndex = drops.length;
    const newColumns = rain.width / fontSize;
    const newRows = rain.height / fontSize;
    for (let x = startIndex; x < newColumns; x += 1) {
      drops.push({
        start: Math.floor(Math.random() * -newRows),
        speed: Math.floor(Math.random() * 3) + 1,
      });
    }
    rain.height = window.innerHeight;
    rain.width = window.innerWidth;

    lead.height = window.innerHeight;
    lead.width = window.innerWidth;
    return drops;
  }

  /**
   * Get text
   */
  function getText(inventory, speed) {
    let text = '';
    for (let index = 0; index < speed; index += 1) {
      const randomIndex = Math.floor((Math.random() * (inventory.length -1)));
      text = text + inventory[randomIndex];
    }
    return text;
  }

  /**
  * Rrawing the characters
  * @constructor
  */
  function draw() {
    // reset lead board
    leadctx.clearRect(0, 0, lead.width, lead.height);

    if (titleProgress >= titleText.length) {
      titleProgress = 0;
      // titlectx.clearRect(0, 0, title.width, title.height);
    }

    if (Math.abs(rain.width - window.innerWidth) > 50) {
      drops = mapDrops(rain, lead, fontSize, drops);
      // prevDrops = drops;
    }

    // Black BG for the canvas
    // translucent BG to show trail
    // ctx.fillStyle = fadeColor;
    // ctx.fillRect(0, 0, rain.width, rain.height);

    ctx.fillStyle = fontColor; // green text
    ctx.font = `${fontWeight} ${fontSize}px ${fontFamily}`;

    leadctx.fillStyle = leadColor; // green text
    leadctx.font = `${leadFontWeight} ${fontSize}px ${fontFamily}`;

    // looping over drop columns
    for (let i = 0; i < drops.length; i += 1) {
      // a random inventory character to print
      const text = getText(inventory, drops[i].speed);
      // x = i*fontSize, y = value of drops[i]*fontSize
      const thisX = i * fontSize;
      const thisY = drops[i].start * fontSize;
      const leadY = thisY + (( drops[i].speed ) * fontSize);
      ctx.fillStyle = leadColor;
      leadctx.fillText(text[0], thisX, leadY);

      leadctx.shadowColor = shadowColor; // string
      // Color of the shadow;  RGB, RGBA, HSL, HEX, and other inputs are valid.
      leadctx.shadowOffsetX = 0; // integer
      // Horizontal distance of the shadow, in relation to the text.
      leadctx.shadowOffsetY = 0; // integer
      // Vertical distance of the shadow, in relation to the text.
      leadctx.shadowBlur = shadowBlur; // integer


      ctx.fillStyle = fontColor; // green text
      for (let n = 0; (n) < text.length; n += 1) {
        ctx.fillStyle = fontColor;
        ctx.font = `${fontWeight} ${fontSize}px ${fontFamily}`;

        ctx.shadowColor = '#ff8080'; // string
        ctx.shadowOffsetX = 0; // integer
        ctx.shadowOffsetY = 0; // integer
        ctx.shadowBlur = 5; // integer
        ctx.fillText(text[n], thisX, thisY + (( n ) * fontSize));
      }

      // clearRect(x,y, width, height);
      ctx.clearRect( thisX, 0, fontSize, (leadY - (rainLength * fontSize)));
      ctx.clearRect( thisX, (leadY + (10 * fontSize)),
          fontSize, rainLength * fontSize);

      if (leadY > rain.height && Math.random() > 0.975) {
        drops[i].start = 0;
        // prevdrops[i].start = 0;
      }

      // incrementing Y coordinate
      drops[i].start += 1 * drops[i].speed;
    }
  }

  drawInterval = setInterval(draw, speed);
};


letitrain();
