// vue.config.js
module.exports = {
  lintOnSave: process.env.NODE_ENV !== 'production',
  // ...other vue-cli plugin options...
  pwa: {
    name: 'Rain in Red | DMC',
    themeColor: '#000',
    msTileColor: '#fff',
    appleMobileWebAppCapable: 'yes',
    appleMobileWebAppStatusBarStyle: 'black',

    // configure the workbox plugin
    workboxPluginMode: 'InjectManifest',
    workboxOptions: {
      // swSrc is required in InjectManifest mode.
      swSrc: 'src/sw.js',
      // ...other Workbox options...
    },
  },
}
